import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'

export default class MoviesController {
    public async store({request, response}: HttpContextContract){
        let newMovies = await Database.table('movies').returning('id').insert({
            title : request.input('title'),
            resume : request.input('resume'),
            release_date : request.input('release_date'),
            genre_id : request.input('genre_id')
            
        })
        response.ok({message: 'created', data: newMovies})
    }
    public async index({response}: HttpContextContract){
        let movie =  await Database.from('movies').select('*')
        response.ok({message: 'success', data: movie})
    }
    public async show({params, response}: HttpContextContract){
        let movie = await Database.from('movies').where('id', params.id).first()
        response.ok({message: 'success', data: movie})
    }
    public async update({params, request, response}: HttpContextContract){
        await Database.from('movies').where('id', params.id).update({
            title : request.input('title'),
            resume : request.input('resume'),
            release_date : request.input('release_date'),
            genre_id : request.input('genre_id')
        })
        response.ok({message: 'updated!'})
    }
    public async destroy({params, response}: HttpContextContract){
        await Database.from('movies').where('id', params.id).delete
        response.ok({message: 'deleted!'})
    }
        
}
