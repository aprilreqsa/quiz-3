import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'

export default class GenresController {
    public async store({request, response}: HttpContextContract){
        let newGenres = await Database.table('genres').returning('id').insert({
            name : request.input('name')
        })
        response.status(200).json({message: 'created', newGenre : newGenres})
    }
    public async index({response}: HttpContextContract){
        let genre = await Database.from('genres').select('*')
        response.status(200).json({message: 'success', data: genre})
    }
    public async show({params, response}: HttpContextContract){
        let genre = await Database.from('genres').where('id', params.id).first()
        response.status(200).json({message:'succes get genres with id', data: genre})
    }
    public async update({params,request, response}: HttpContextContract){
        await Database.from('genres').where('id', params.id).update({
            name : request.input('name')
        })
        response.status(200).json({message:'updated!'})
    }
    public async destroy({params,response}: HttpContextContract){
        await Database.from('genres').where('id', params.id).delete()
        response.status(200).json({message: 'deleted!'})
    }
}
